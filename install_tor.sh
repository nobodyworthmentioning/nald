#!/bin/bash

sudo apt install tor git -y
cd /opt
sudo mkdir nipe
sudo chown `whoami`:`whoami` `pwd`/nipe
cd nipe
git clone https://github.com/GouveaHeitor/nipe .
sudo cpan install Switch JSON Config::Simple
perl nipe.pl install

sudo rm /opt/onboot
echo "#!/bin/bash
nald_tor_restart
curl ifconfig.co
sleep 10;
exit 0" | sudo tee -a /opt/onboot > /dev/null
sudo chmod +x /opt/onboot

sudo rm /bin/nald_tor_restart
echo "#!/bin/bash
cd /opt/nipe
sudo service tor restart
perl nipe.pl restart" | sudo tee -a /bin/nald_tor_restart > /dev/null
sudo chmod +x /bin/nald_tor_restart
cd $HOME
mkdir -p ./.config/autostart
cd .config/autostart
echo "[Desktop Entry]
Type=Application
Exec=/opt/onboot
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name=onboot" > onboot.desktop


nald_tor_restart
